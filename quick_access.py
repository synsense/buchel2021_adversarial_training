from Experiments import (
    weight_scale_experiment,
    mismatch_experiment,
    methods_experiment,
    worst_case_experiment,
    landscape_experiment,
    methods_random_experiment,
    IBP_experiment,
    landscape_vary_beta_experiment,
    methods_random_vary_beta_experiment)

# mismatch_experiment.mismatch_experiment.visualize()
# worst_case_experiment.worst_case_experiment.visualize()
# methods_experiment.methods_experiment.visualize()
# weight_scale_experiment.weight_scale_experiment.visualize()
# landscape_experiment.landscape_experiment.visualize()
# methods_random_experiment.methods_random_experiment.visualize()
# IBP_experiment.IBP_experiment.visualize()
landscape_vary_beta_experiment.landscape_vary_beta_experiment.visualize()
# methods_random_vary_beta_experiment.methods_random_vary_beta_experiment.visualize()